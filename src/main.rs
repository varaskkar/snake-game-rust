extern crate termion;

use termion::*;
use termion::raw::IntoRawMode;
use termion::async_stdin;
use std::io::{Read, Write, stdout};
use std::{thread, time};
// use termion::event::Key;
// use termion::input::TermRead;

static mut HALT: bool = false;
static mut PAUSE: bool = false;
static mut AXIS_X: u16 = 3;
static mut AXIS_Y: u16 = 6;

fn main() {
    unsafe{
        let stdout = stdout();
        let mut stdout = stdout.lock().into_raw_mode().unwrap();
        let mut stdin = async_stdin();
        let (size_window_x, size_window_y) = terminal_size().unwrap();

        println!("{}", termion::cursor::Hide);                                          // Hide cursor (Show to do it visible)
        // println!("{}", size_window_x);
        // println!("{}", size_window_y);
        loop {
            thread::sleep(time::Duration::from_millis(60));
            // println!("{}", termion::clear::All);
            write!(stdout, "{}", termion::clear::All).unwrap();
            main_screen();
            async_reading_keyboard(&mut stdin);

            if HALT == true{ break; }
            if !PAUSE{
                println!("{blue}", blue = color::Fg(color::Blue) );
                println!("{}*", termion::cursor::Goto(AXIS_X, AXIS_Y));

                println!("{reset}{}", reset = color::Fg(color::Reset));
            }else{
                println!("{} PAUSE", termion::cursor::Goto(size_window_x/2 - 5, size_window_y/2));
            }


            // Animacion
            // Leer de teclado (poner tecla izq, derecha, ... para mover el gusano)
            // No salirme del mapa
        }
    }
}

fn main_screen(){
    println!("{red}", red = color::Fg(color::Red) );
    println!("{} ╔═╗┌┐┌┌─┐┬┌─┌─┐  ╔═╗┌─┐┌┬┐┌─┐", termion::cursor::Goto(2, 2));      // Column and row
    println!("{} ╚═╗│││├─┤├┴┐├┤   ║ ╦├─┤│││├┤ ", termion::cursor::Goto(2, 3));
    println!("{} ╚═╝┘└┘┴ ┴┴ ┴└─┘  ╚═╝┴ ┴┴ ┴└─┘", termion::cursor::Goto(2, 4));
    println!("{reset}{}", reset = color::Fg(color::Reset));
}

fn async_reading_keyboard(stdin: &mut AsyncReader){

    let read = stdin.bytes().next();

    unsafe{
        if let Some(Ok(b'q')) = read {
            HALT = true;
        }else if let Some(Ok(b'p')) = read {
            PAUSE = !PAUSE;
        }else if let Some(Ok(b'a')) = read {
            AXIS_X = AXIS_X - 1;
        }else if let Some(Ok(b'd')) = read {
            AXIS_X = AXIS_X + 1;
        }else if let Some(Ok(b'w')) = read {
            AXIS_Y = AXIS_Y - 1;
        }else if let Some(Ok(b's')) = read {
            AXIS_Y = AXIS_Y + 1;
        }

        else if let Some(Ok(37)) = read {
            AXIS_X = AXIS_X - 1;
        }else if let Some(Ok(39)) = read {
            AXIS_X = AXIS_X + 1;
        }else if let Some(Ok(38)) = read {
            AXIS_Y = AXIS_Y - 1;
        }else if let Some(Ok(40)) = read {
            AXIS_Y = AXIS_Y + 1;
        }
    }
}